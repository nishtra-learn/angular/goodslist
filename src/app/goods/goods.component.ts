import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.css']
})
export class GoodsComponent implements OnInit {
  goods: string[] = [];

  constructor(private httpService: HttpService) { }

  ngOnInit(): void {
    this.httpService
    .getGoodsList()
    .subscribe((data) => {
      console.log(data);
      
      this.goods = data['goods'];
    });
  }

}
